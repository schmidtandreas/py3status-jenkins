Introduction
============

The `py3status-jenkins-status <https://gitlab.com/schmidtandreas/py3status-jenkins>`_ is a `py3status <https://github.com/ultrabug/py3status>`_ module.
It displays the status of `Jenkins <https://www.jenkins.io/>`_ jobs.
Each configured instance of the module represent a jenkins server and can displays several jobs running on appropriate server.
Of course it is passible to configure several instance of this module to observe jobs on different jenkins servers.

Installation
------------

You can install `py3status-jenkins-status <https://gitlab.com/schmidtandreas/py3status-jenkins>`_ module by using the pip module.

.. code-block:: shell

   python -m pip install py3status-jenkins-status

Support
-------

Get help, share ideas or feedback, report bugs, or others, see:

GitLab
^^^^^^

`Issues <https://gitlab.com/schmidtandreas/py3status-jenkins/-/issues>`_ /
`Pull/Merge requests <https://gitlab.com/schmidtandreas/py3status-jenkins/-/merge_requests>`_
