.. py3status-jenkins-status documentation master file, created by
   sphinx-quickstart on Tue Apr 27 21:43:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to py3status-jenkins-status's documentation!
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   jenkins_status

   changelog
