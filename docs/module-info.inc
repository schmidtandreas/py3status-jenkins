
.. _module_jenkins_status:

jenkins_status
--------------

Displays status of jenkins jobs.

This py3status module displays the status of desired jenkins jobs.
The desired jenkins jobs are defined by configuration. The current
status of each job is represented by the i3status colors. In default
configuration of the colors is as following:

Configuration parameters:
  - ``cache_timeout`` Renew of jenkins job status interval in seconds *(default 60)*
  - ``jenkins_url`` The base URL of jenkins server
  - ``jenkins_pass_path`` Path to jenkins credentials, if password-store is using.
  - ``jenkins_username`` Username for jenkins server, if unsecure credentials storage method is using.
  - ``jenkins_password`` Password for jenkins server, if unsecure credentials storage method is using.
  - ``text_status`` Text displayed for jenkins job status (default "S").
  - ``text_not_connected`` Text displayed if jenkins server is not reachable (default "-").
  - ``text_common_error`` Text displayed if an error occurred (default "E").
  - ``text_job_separator`` Separator between the Jenkins job status.
  - ``jobs`` List of jenkins jobs.

Configuration parameter for jobs entries:
  - ``name`` Jenkins job name.
  - ``text_status`` Text dispayls for this jenkins job status. If not set, the common text_status will be displayed.

Format placeholders:

    format: {jobs} dispay the configured jenkins jobs status
    format_job: {status} display the status of a jenkins job

Color options:
  - ``color_good`` job status "SUCCESS"
  - ``color_degraded`` job status "UNSTABIL"
  - ``color_bad`` job status "FAILURE"
  - ``color_bad`` common error or jenkins server not reachable

Requires:
  - ``pass`` Recommended storage of jenkins credentials.

Example:


.. code-block:: py3status

    jenkins_status {
      jenkins_url = "https://myjenkins:1234"
      jenkins_pass_path = "Jenkins"
      jobs = [
        {
          "name": "job1",
          "text_status": "J1"
        },
        {
          "name": "job2",
          "text_status": "J2"
        }
      ]
    }


**author** Andreas Schmidt

**license** MIT
