.. _configuration:

Configuration
=============

Usually you can use the `py3status-jenkins-status <https://gitlab.com/schmidtandreas/py3status-jenkins>`_ module immediately after the installation.
If you install the module in another way, you can add the path of the module to `py3status <https://github.com/ultrabug/py3status>`_ as described in `py3status documentation <https://py3status.readthedocs.io/en/latest/configuration.html#configuration>`_

Configuring py3status-jenkins-status module
-------------------------------------------
As other i3status modules, the py3stats-jenkins-status module can be configured directly in ``i3status.conf`` file by adding the module entry point ``jenkins_status`` like this:

.. code-block:: py3status
   :caption: Example

   order += "jenkins_status"

   jenkins_status {
       jenkins_url = "https://my-jenkins-server:8888"
       jenkins_username = "user"
       jenkins_password = ""
       jobs = [
         {
           "name": "job1",
           "text_status": "J1"
         },
         {
           "name": "job2"
         }
       ]
   }

Common options
^^^^^^^^^^^^^^
- ``format``: Specify the output format. Use ``{status}`` as placeholder for the jobs stauts outputs.
- ``job_separator``: Specify the separator between the jenkins job status.
- ``jenkins_url``: Specify jenkins base server address.
- ``text_status``: Specify default status text, colored in depending of jenkins job status. The default value is ``S``.
- ``text_not_connected``: Specify default output text if jenkins server not reachable. The default value is ``-``.
- ``text_error``: Specify default output if any error occurred. The default value is ``E``.
- ``cache_time``: Specify the interval of jenkins job status update in seconds. The default value is ``60`` seconds.
- ``jobs``: The list of the diplayed jenkins jobs.
    - ``name``: The name of the jenkins job.
    - ``text_status``: Specify the status text for the specific jenkins job.

Credentials options
^^^^^^^^^^^^^^^^^^^
The py3status-jenkins-status module supports currently two different methods to manage the credentials for jenkins server access.
The first method is store the credentials in configuration file as plain text.
This method is highly **unsafe** and is not recommended to use.
If you still decide to use this method, you can use the following option to set the access data:

- ``jenkins_username``: Specify the username of jenkins credentials.
- ``jenkins_password``: Specify the password of jenkins credentials.

The second method is to store the jenkins credentials in `password-storage <https://www.passwordstore.org/>`_. This method
is more secure and is recommended to use.

- ``jenkins_pass_path``: Specify the path to jenkins credentials stored in `password-storage <https://www.passwordstore.org/>`_

.. note::
   If you configure both methods, the more secure method will be preferred.
   Be aware, that in such case the methods are **not** redundant and if the password-storage method fail, then the module output an error.

