[![Documentation Status](https://readthedocs.org/projects/py3status-jenkins/badge/?version=latest)](https://py3status-jenkins.readthedocs.io/en/latest/?badge=latest)
[![coverage report](https://gitlab.com/schmidtandreas/py3status-jenkins/badges/master/coverage.svg)](https://gitlab.com/schmidtandreas/py3status-jenkins/-/commits/master)
[![pipeline status](https://gitlab.com/schmidtandreas/py3status-jenkins/badges/master/pipeline.svg)](https://gitlab.com/schmidtandreas/py3status-jenkins/-/commits/master)

# py3status-jenkins-status

A [py3status](https://github.com/ultrabug/py3status) module displays the current status of jenkins jobs.
