from py3status import docstrings as py3docstrings
from pathlib import Path

def _markdown_2_rst(lines):
    """
    Convert markdown to restructured text
    """
    out = []
    code = False
    for line in lines:
        # code blocks
        if line.strip() == "```":
            code = not code
            space = " " * (len(line.rstrip()) - 3)
            if code:
                out.append(f"\n\n{space}.. code-block:: py3status\n\n")
            else:
                out.append("\n")
        else:
            if code and line.strip():
                line = "    " + line
            else:
                # escape any backslashes
                line = line.replace("\\", "\\\\")
            out.append(line)
    return out

def create_auto_documentation():
    config = {
        "include_paths": [ Path("../src/py3status_jenkins_status") ]
    }
    data = py3docstrings.core_module_docstrings(include_core=False, include_user=True, config=config, format="rst")

    out = []
    # details
    for module in sorted(data):
        out.append(f"\n.. _module_{module}:\n")  # reference for linking
        out.append(
            "\n{name}\n{underline}\n\n{screenshots}{details}\n".format(
                name=module,
                screenshots="",
                underline="-" * len(module),
                details="".join(_markdown_2_rst(data[module])).strip(),
            )
        )
    # write include file
    Path("module-info.inc").write_text("".join(out))
