from py3status_jenkins_status.jenkins_status import Py3status
from unittest.mock import patch
import unittest


class TestPassCredentials(unittest.TestCase):
    @patch("py3status_jenkins_status.jenkins_status.PasswordStore")
    def test_pass_credentials_exception(self, mock_pass):
        mock_pass.side_effect = Exception("Something went wrong")
        js = Py3status()
        js.jenkins_pass_path = "credentials_path"
        user, pw = js._get_pass_credentials()
        mock_pass.get_passwords_list.assert_not_called()
        mock_pass.get_decrypted_password.assert_not_called()
        assert user is None and pw is None

    @patch("py3status_jenkins_status.jenkins_status.PasswordStore")
    def test_pass_credentials_empty_path(self, mock_pass):
        mock_pass.return_value = mock_pass
        mock_pass.get_passwords_list.return_value = ["credentials_path"]
        js = Py3status()
        js.jenkins_pass_path = ""
        user, pw = js._get_pass_credentials()
        mock_pass.get_passwords_list.assert_called()
        mock_pass.get_decrypted_password.assert_not_called()
        assert user is None and pw is None

    @patch("py3status_jenkins_status.jenkins_status.PasswordStore")
    def test_pass_credentials_invalid_path(self, mock_pass):
        mock_pass.return_value = mock_pass
        mock_pass.get_passwords_list.return_value = ["credentials_path"]
        js = Py3status()
        js.jenkins_pass_path = "invalid_path"
        user, pw = js._get_pass_credentials()
        mock_pass.get_passwords_list.assert_called()
        mock_pass.get_decrypted_password.assert_not_called()
        assert user is None and pw is None

    @patch("py3status_jenkins_status.jenkins_status.PasswordStore")
    def test_pass_credentials_success(self, mock_pass):
        mock_pass.return_value = mock_pass
        mock_pass.get_passwords_list.return_value = ["credentials_path"]
        mock_pass.get_decrypted_password.side_effect = ["username", "password"]
        js = Py3status()
        js.jenkins_pass_path = "credentials_path"
        user, pw = js._get_pass_credentials()
        mock_pass.get_passwords_list.assert_called()
        mock_pass.get_decrypted_password.assert_called()
        assert user == "username"
        assert pw == "password"


class TestCredentials(unittest.TestCase):
    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_pass_credentials")
    def test_get_credentials_not_defined(self, mock_get_pass_credentions):
        js = Py3status()
        user, pw = js._get_credentials()
        mock_get_pass_credentions.assert_not_called()
        assert user is None and pw is None

    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_pass_credentials")
    def test_get_credentials_with_pass_path(self, mock_get_pass_credentions):
        mock_get_pass_credentions.return_value = ("username", "password")
        js = Py3status()
        js.jenkins_pass_path = "credentials_path"
        user, pw = js._get_credentials()
        mock_get_pass_credentions.assert_called()
        assert user == "username"
        assert pw == "password"

    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_pass_credentials")
    def test_get_credentials_with_pass_path_and_username(
        self, mock_get_pass_credentions
    ):
        mock_get_pass_credentions.return_value = ("username", "password")
        js = Py3status()
        js.jenkins_pass_path = "credentials_path"
        js.jenkins_username = "username"
        js.jenkins_password = "password"
        user, pw = js._get_credentials()
        mock_get_pass_credentions.assert_called()
        assert user == "username"
        assert pw == "password"

    def test_get_credentials_username(self):
        js = Py3status()
        js.jenkins_username = "username"
        js.jenkins_password = "password"
        user, pw = js._get_credentials()
        assert user == "username"
        assert pw == "password"
