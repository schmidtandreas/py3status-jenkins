from py3status_jenkins_status.jenkins_status import Py3status
from unittest.mock import MagicMock
import unittest


class TestErrorResponse(unittest.TestCase):
    color_bad = '#FF0000'

    def test_error_response(self):
        error_text = "any error text"
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        mock_py3.safe_format.return_value = error_text
        expected_response = {"full_text": error_text, "color": self.color_bad}
        py3status = Py3status()
        py3status.py3 = mock_py3
        error_response = py3status._error_response(error_text)
        assert isinstance(error_response, dict)
        assert expected_response == error_response
