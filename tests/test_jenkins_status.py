from py3status_jenkins_status.jenkins_status import Py3status
from unittest.mock import patch, MagicMock
import unittest
from py3status.composite import Composite


class TestErrorResponse(unittest.TestCase):
    color_bad = '#FF0000'
    color_good = '#00FF00'
    error_response = {'full_text': "E", "color": color_bad}

    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_set_jenkins_server_failed(self, mock_error_response, mock_set_jenkins_server):
        mock_set_jenkins_server.return_value = False
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        py3status = Py3status()
        py3status.py3 = mock_py3
        response = py3status.jenkins_status()
        assert response == self.error_response

    @patch("py3status_jenkins_status.jenkins_status.Py3status._is_jenkins_available")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_jenkins_not_available(self,
                                                  mock_error_response,
                                                  mock_set_jenkins_server,
                                                  mock_is_jenkins_available):
        mock_set_jenkins_server.return_value = True
        mock_is_jenkins_available.return_value = False
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        py3status = Py3status()
        py3status.py3 = mock_py3
        response = py3status.jenkins_status()
        assert response == self.error_response

    @patch("py3status_jenkins_status.jenkins_status.Py3status._is_jenkins_available")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_jobs_not_set(self,
                                         mock_error_response,
                                         mock_set_jenkins_server,
                                         mock_is_jenkins_available):
        mock_set_jenkins_server.return_value = True
        mock_is_jenkins_available.return_value = True
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status.jobs = None
        response = py3status.jenkins_status()
        assert response == self.error_response

    @patch("py3status_jenkins_status.jenkins_status.Py3status._is_jenkins_available")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_job_not_set(self,
                                        mock_error_response,
                                        mock_set_jenkins_server,
                                        mock_is_jenkins_available):
        mock_set_jenkins_server.return_value = True
        mock_is_jenkins_available.return_value = True
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status.jobs = [None, None]
        response = py3status.jenkins_status()
        assert response == self.error_response

    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_job_color")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._is_jenkins_available")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_job_failed(self,
                                       mock_error_response,
                                       mock_set_jenkins_server,
                                       mock_is_jenkins_available,
                                       mock_get_job_color):
        mock_set_jenkins_server.return_value = True
        mock_is_jenkins_available.return_value = True
        mock_get_job_color.return_value = (False, self.color_bad)
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        # TODO use real py3 helper functions
        mock_py3.safe_format.side_effect = ["E", "", "JS: E"]
        mock_py3.time_in.return_value = 12345
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status.jobs = [{'name': 'job1'}]
        response = py3status.jenkins_status()
        assert response == {"cached_until": 12345, "full_text": "JS: E"}

    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_job_color")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._is_jenkins_available")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_several_job_failed(self,
                                               mock_error_response,
                                               mock_set_jenkins_server,
                                               mock_is_jenkins_available,
                                               mock_get_job_color):
        mock_set_jenkins_server.return_value = True
        mock_is_jenkins_available.return_value = True
        mock_get_job_color.return_value = (False, self.color_bad)
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        # TODO use real py3 helper functions
        mock_py3.safe_format.side_effect = ["E", "E", "", "JS: EE"]
        mock_py3.time_in.return_value = 12345
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status.jobs = [{"name": "job1"}, {"name": "job2"}]
        response = py3status.jenkins_status()
        assert response == {"cached_until": 12345, "full_text": "JS: EE"}

    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_job_color")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._is_jenkins_available")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_several_jobs_success(self,
                                                 mock_error_response,
                                                 mock_set_jenkins_server,
                                                 mock_is_jenkins_available,
                                                 mock_get_job_color):
        mock_set_jenkins_server.return_value = True
        mock_is_jenkins_available.return_value = True
        mock_get_job_color.side_effect = [(True, self.color_good), (True, self.color_good)]
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        # TODO use real py3 helper functions
        mock_py3.safe_format.side_effect = ["S", "S", "", {"full_text": "JS: SS", "color": self.color_good}]
        mock_py3.time_in.return_value = 12345
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status.jobs = [{"name": "job1"}, {"name": "job2"}]
        response = py3status.jenkins_status()
        assert response == {"cached_until": 12345, "full_text": {"full_text": "JS: SS", "color": self.color_good}}

    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_job_color")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._is_jenkins_available")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._set_jenkins_server")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._error_response")
    def test_jenkins_status_several_compiste_jobs_success(self,
                                                          mock_error_response,
                                                          mock_set_jenkins_server,
                                                          mock_is_jenkins_available,
                                                          mock_get_job_color):
        mock_set_jenkins_server.return_value = True
        mock_is_jenkins_available.return_value = True
        mock_get_job_color.side_effect = [(True, self.color_good), (True, self.color_good)]
        mock_error_response.return_value = self.error_response
        mock_py3 = MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        # TODO use real py3 helper functions
        mock_py3.safe_format.side_effect = [Composite("S"), "S", "", {"full_text": "JS: SS", "color": self.color_good}]
        mock_py3.time_in.return_value = 12345
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status.jobs = [{"name": "job1"}, {"name": "job2"}]
        response = py3status.jenkins_status()
        assert response == {"cached_until": 12345, "full_text": {"full_text": "JS: SS", "color": self.color_good}}
