from pkg_resources import parse_version
from py3status_jenkins_status.version import version as py3_js_version
import re


def test_py3status_version():
    import py3status.version

    assert parse_version(py3status.version.version) >= parse_version("3.20")


def test_py3status_jenkins_status_version():
    pattern = re.compile(r"[0-9]*\.[0-9]*\.[0-9]*")
    assert pattern.match(py3_js_version)
