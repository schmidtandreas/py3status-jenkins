from py3status_jenkins_status.jenkins_status import Py3status
from unittest.mock import patch
import unittest


class TestJenkins(unittest.TestCase):
    def test_set_jenkins_server_already_set(self):
        py3status = Py3status()
        py3status._jenkins_server = object()
        assert py3status._set_jenkins_server()

    @patch("py3status_jenkins_status.jenkins_status.Jenkins")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_credentials")
    def test_set_jenkins_server_invalid_credentials(
        self, mock_get_credentials, mock_jenkins
    ):
        mock_get_credentials.side_effect = [
            (None, None),
            ("username", None),
            (None, "password"),
        ]
        py3status = Py3status()
        assert not py3status._set_jenkins_server()
        assert not py3status._set_jenkins_server()
        assert not py3status._set_jenkins_server()

    @patch("py3status_jenkins_status.jenkins_status.Jenkins")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_credentials")
    def test_set_jenkins_server_exception_occurred(
        self, mock_get_credentials, mock_jenkins
    ):

        mock_get_credentials.return_value = ("username", "password")
        mock_jenkins.return_value = mock_jenkins
        mock_jenkins.side_effect = Exception("something went wrong")
        py3status = Py3status()
        assert not py3status._set_jenkins_server()

    @patch("py3status_jenkins_status.jenkins_status.Jenkins")
    @patch("py3status_jenkins_status.jenkins_status.Py3status._get_credentials")
    def test_set_jenkins_server_success(self, mock_get_credentials, mock_jenkins):
        mock_get_credentials.return_value = ("username", "password")
        mock_jenkins.return_value = mock_jenkins
        py3status = Py3status()
        assert py3status._set_jenkins_server()

    def test_jenkins_server_not_available(self):
        mock_jenkins = unittest.mock.MagicMock()
        mock_jenkins.get_whoami.side_effect = Exception("Something went wrong")
        py3status = Py3status()
        py3status._jenkins_server = mock_jenkins
        assert not py3status._is_jenkins_available()

    def test_jenkins_server_is_available(self):
        mock_jenkins = unittest.mock.MagicMock()
        mock_jenkins.get_whoami.return_value = "I am a valid response"
        py3status = Py3status()
        py3status._jenkins_server = mock_jenkins
        assert py3status._is_jenkins_available()
        mock_jenkins.get_whoami.assert_called()


class TestJenkinsJobColor(unittest.TestCase):
    color_bad = "#FF0000"
    color_good = "#00FF00"
    color_degraded = "#FFFF00"
    color_aborted = "#CCCCCC"

    def assert_error_response(self, success: bool, color: str):
        assert not success
        assert color == self.color_bad

    def test_get_job_color_invalid_job_key(self):
        mock_py3 = unittest.mock.MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        job = {"invalid key": "some content"}
        py3status = Py3status()
        py3status.py3 = mock_py3
        success, color = py3status._get_job_color(job)
        self.assert_error_response(success, color)

    def test_get_job_color_invalid_job_name(self):
        mock_jenkins = unittest.mock.MagicMock()
        mock_jenkins.get_job_info.side_effect = Exception("job not found")
        mock_py3 = unittest.mock.MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        job = {"name": "not existing job"}
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status._jenkins_server = mock_jenkins
        success, color = py3status._get_job_color(job)
        self.assert_error_response(success, color)

    def test_get_job_color_invalid_job_info(self):
        mock_jenkins = unittest.mock.MagicMock()
        mock_jenkins.get_job_info.return_value = {"some_key": "some value"}
        mock_py3 = unittest.mock.MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        job = {"name": "job"}
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status._jenkins_server = mock_jenkins
        success, color = py3status._get_job_color(job)
        self.assert_error_response(success, color)
        mock_jenkins.get_job_info.assert_called_with("job")

    def test_get_job_color_invalid_build_number(self):
        mock_jenkins = unittest.mock.MagicMock()
        mock_jenkins.get_job_info.return_value = {"lastCompletedBuild": {"number": "4"}}
        mock_jenkins.get_build_info.return_value = {"invalid_key": "some value"}
        mock_py3 = unittest.mock.MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        job = {"name": "job"}
        py3status = Py3status()
        py3status.py3 = mock_py3
        py3status._jenkins_server = mock_jenkins
        success, color = py3status._get_job_color(job)
        self.assert_error_response(success, color)
        mock_jenkins.get_job_info.assert_called_with("job")
        mock_jenkins.get_build_info.assert_called_with("job", "4")

    def test_get_job_color_results(self):
        mock_jenkins = unittest.mock.MagicMock()
        mock_jenkins.get_job_info.return_value = {"lastCompletedBuild": {"number": "4"}}
        mock_jenkins.get_build_info.side_effect = [
            {"result": "SUCCESS"},
            {"result": "UNSTABLE"},
            {"result": "FAILURE"},
            {"result": "ABORTED"},
            {"result": "ABORTED"},
        ]
        mock_py3 = unittest.mock.MagicMock()
        mock_py3.COLOR_BAD = self.color_bad
        mock_py3.COLOR_GOOD = self.color_good
        mock_py3.COLOR_DEGRADED = self.color_degraded
        mock_py3.COLOR_ABORTED = self.color_aborted
        job = {"name": "job"}
        py3status = Py3status()
        py3status.color_aborted = self.color_aborted
        py3status.py3 = mock_py3
        py3status._jenkins_server = mock_jenkins
        success, color = py3status._get_job_color(job)
        assert success
        assert color == self.color_good
        success, color = py3status._get_job_color(job)
        assert success
        assert color == self.color_degraded
        success, color = py3status._get_job_color(job)
        assert success
        assert color == self.color_bad
        success, color = py3status._get_job_color(job)
        assert success
        assert color == self.color_aborted
        mock_py3.COLOR_ABORTED = None
        success, color = py3status._get_job_color(job)
        assert success
        assert color == self.color_aborted
